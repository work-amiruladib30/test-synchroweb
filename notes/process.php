<?php
// Database Connection ###############	
include_once("../config/db_connection.php"); 


if(isset($_POST['action'])){

	/**
     * Store new record.
     * Validation form check field.
     * 
     */
	if($_POST['action'] == 'store'){

		## START Validation form error
		if(empty(trim($_POST['title']))){

			$response_array['status'] = 'error';  
			$response_array['message'][] = 'Title field is required.';  
		}else if(strlen($_POST['title']) <= 4){

			$response_array['status'] = 'error';  
			$response_array['message'][] = 'Title field min length is 5.';  
		}

		if(empty(trim($_POST['description']))){

			$response_array['status'] = 'error';  
			$response_array['message'][] = 'Description field is required.';  
		}else if(strlen($_POST['description']) <= 4){
			$response_array['status'] = 'error';  
			$response_array['message'][] = 'Description field min length is 5.';  
		}
		## END Validation form
		

		if(!isset($response_array['status'])){

			$title = $_POST['title'];
			$description = $_POST['description'];
			$created_at = date("Y-m-d H:i:s");

			$sql = "INSERT INTO notes (title, description, created_at, updated_at) VALUES ('$title', '$description', '$created_at', '$created_at')";

			$result = mysqli_query($conn, $sql);

			if($result){
				$response_array['status'] = 'success';  
				$response_array['message'] = 'New record sunccessfully added.';  
			}else{
				$response_array['status'] = 'error';  
				$response_array['message'] = 'Error. Please contact our support';
			}
		}

		## RETURN Response
		header('Content-type: application/json');
		echo json_encode($response_array);


	}


	/**
     * View Record.
     */
	if($_POST['action'] == 'index'){

		$sql = "SELECT * FROM notes WHERE deleted_at IS NULL ORDER BY created_at DESC";
		$results = mysqli_query($conn, $sql);

		$table_row = '';
		if (mysqli_num_rows($results) > 0){
			while($row = mysqli_fetch_assoc($results)){

				$table_row .= '<tr>';
				$table_row .= '<td>' . ($row['is_done'] ? '<strike>' : '') . $row['title'] . ($row['is_done'] ? '</strike>' : '') . '</td>';
				$table_row .= '<td>';
				$table_row .= '<button class="btn btn-simple btn-info btn-icon" onclick="showModal(' .$row['id']. ')"><i class="ti-eye"></i></button>';
				$table_row .= '<button class="btn btn-simple btn-info btn-icon" onclick="isDone(' .$row['id']. ')"><i class="ti-pencil-alt"></i></button>';
				$table_row .= '<button class="btn btn-simple btn-info btn-icon" onclick="deleteRow(' .$row['id']. ')"><i class="ti-trash"></i></button>';
				$table_row .= '</td>';
				$table_row .= '</tr>';
			}
		}else{

				$table_row .= '<tr>';
				$table_row .= '<td colspan="2" class="text-center">No Results</td>';
				$table_row .= '</tr>';
		}

		echo $table_row;

	}


	/**
     * Upldate Record.
     */
	if($_POST['action'] == 'update'){

		$sql = "SELECT * FROM notes WHERE id = " . $_POST['id'];
		$results = mysqli_query($conn, $sql);
		$row = mysqli_num_rows($results);
		$data = mysqli_fetch_assoc($results);
		
		if($row){

			$sql_update = "UPDATE notes set is_done = " . ($data['is_done'] ? 0 : 1) . "  WHERE id = " . $_POST['id'];;
			$result_update = mysqli_query($conn, $sql_update);

			if($result_update){
				$response_array['status'] = 'success';  
				$response_array['message'] = 'Record sunccessfully update.';  
			}else{
				$response_array['status'] = 'error';  
				$response_array['message'] = 'Error. Please contact our support';
			}


		}else{
			$response_array['status'] = 'error';  
			$response_array['message'] = 'Error. Record not found.';
		}



		header('Content-type: application/json');
		echo json_encode($response_array);

	}

	/**
     * Delete Record.
     */
	if($_POST['action'] == 'delete'){

		$sql = "SELECT * FROM notes WHERE id = " . $_POST['id'];
		$results = mysqli_query($conn, $sql);
		$row = mysqli_num_rows($results);
		$data = mysqli_fetch_assoc($results);
				
		if($row){

			## ADD DELETED AT FIELD FOR RECORD
			$deleted_at = date("Y-m-d H:i:s");
			$sql_update = "UPDATE notes set deleted_at = '" . $deleted_at . "'  WHERE id = " . $_POST['id'];
			$result_update = mysqli_query($conn, $sql_update);
			// ## ADD DELETED AT FIELD FOR RECORD

			if($result_update){
				$response_array['status'] = 'success';  
				$response_array['message'] = 'Record sunccessfully delete.';  
			}else{
				$response_array['status'] = 'error';  
				$response_array['message'] = 'Error. Please contact our support';
			}


		}else{
			$response_array['status'] = 'error';  
			$response_array['message'] = 'Error. Record not found.';
		}


		header('Content-type: application/json');
		echo json_encode($response_array);

	}


	/**
     * show Record.
     */
	if($_POST['action'] == 'show'){
		$sql = "SELECT * FROM notes WHERE id = " . $_POST['id'];
		$results = mysqli_query($conn, $sql);
		$row = mysqli_num_rows($results);
		$data = mysqli_fetch_assoc($results);
			
		if($row == 1){
			header('Content-type: application/json');

			$data['title'] = $data['is_done'] ? ('<strike>' . $data['title'] . '</strike>') : $data['title'];
 
			echo json_encode($data);

		}else{
			$response_array['status'] = 'error';  
			$response_array['message'] = 'Error. Record not found.';

			header('Content-type: application/json');
			echo json_encode($response_array);
		}


	}

}

?>