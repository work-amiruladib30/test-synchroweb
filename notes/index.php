<?php
	// Renama Page Name
	$page_name = 'Note List';
    include_once("../layouts/paper-dashboards/header.php");
?>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
    	<div class="card">
            <div class="card-content">
            	<form id="form-add-note">
	            	<div class="row">
	            		<div class="col-md-8 col-xs-8">
	            			<input type="text" name="title" class="form-control" value="" id="title" placeholder="Title" autocomplete="off">
	            			<textarea class="form-control" name="description" placeholder="Write note here ..."></textarea>
	            		</div>
	            		<div class="col-md-4 col-xs-4">
	    					<button class="btn btn-primary btn-fill" type="submit">Add New</button>
	            		</div>
	            	</div>
            	</form>
            </div>
        </div>
        <div class="card">
            <div class="card-content">
                <div class="fresh-datatables">
					<table id="" class="table table-striped" cellspacing="0" width="100%" style="width:100%">
					<thead>
						<tr>
							<th>Note</th>
							<th class="disabled-sorting">Actions</th>
						</tr>
					</thead>
					<tbody id="view-row-note">
				   	</tbody>
				    </table>
				</div>


            </div>
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->


<div class="modal" tabindex="-1" role="dialog" id="NoteModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modal-note-title"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p id="modal-note-createdat"></p>
				<p id="modal-note-desc"></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php
    include_once("../layouts/paper-dashboards/footer.php");
?>
<script type="text/javascript">
    $(document).ready(function() {

    	loadTable();

    	// SUBMIT form note
    	$("#form-add-note").submit(function(e){
    		e.preventDefault();


    		$.ajax({
	           	type: 'POST',
	            url	: 'process.php',
	            data: $('#form-add-note').serialize() + "&action=store",
	            success: function (data) {
					if(data.status == 'success'){
						displayMessage(data.message)
						$('#form-add-note').trigger("reset");
						loadTable();
					}else if(data.status == 'error'){

						data.message.forEach(function (msg) { 
							displayMessage(msg, 'danger')
				        }); 
					}
	            }
	        });
    	})

    });

	function loadTable() {
		$.ajax({
           	type: 'POST',
            url	: 'process.php',
            data: {'action' : 'index'},
            success: function (data) {
            	
            	$("#view-row-note").html(data);
				
            }
        });
	}

	function isDone(row_id) {
		$.ajax({
           	type: 'POST',
            url	: 'process.php',
            data: {'action' : 'update', 'id' : row_id},
            success: function (data) {
				displayMessage(data.message, data.status)
				loadTable();
            }
        });
	}

	function deleteRow(row_id) {
		$.ajax({
           	type: 'POST',
            url	: 'process.php',
            data: {'action' : 'delete', 'id' : row_id},
            success: function (data) {
				displayMessage(data.message, data.status)
				loadTable();
            }
        });
	}

	function showModal(row_id) {
		console.log(row_id);
		$.ajax({
           	type: 'POST',
            url	: 'process.php',
            data: {'action' : 'show', 'id' : row_id},
            success: function (data) {
            	if(data.status == 'error'){
					displayMessage(data.message, 'danger', true)
				}else{
					$("#modal-note-title").html(data.title)
					$("#modal-note-createdat").html('Created At : ' + data.created_at)
					$("#modal-note-desc").html(data.description)
					$("#NoteModal").modal('toggle');
				}


            }
        });
	}
</script>