<?php
	session_start();

	date_default_timezone_set("Asia/Kuala_Lumpur");

	##Master connection
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "test-synchroweb";

	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	// Check connection
	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
}
?>