<!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js   -->
<script src="../../templates/paper-dashboards/js/jquery.min.js" type="text/javascript"></script>
<script src="../../templates/paper-dashboards/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="../../templates/paper-dashboards/js/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="../../templates/paper-dashboards/js/bootstrap.min.js" type="text/javascript"></script>


<!--  Switch and Tags Input Plugins -->
<script src="../../templates/paper-dashboards/js/bootstrap-switch-tags.js"></script>


<!--  Charts Plugin -->
<script src="../../templates/paper-dashboards/js/chartist.min.js"></script>
<!--  Plugin for DataTables.net  -->
<script src="../../templates/paper-dashboards/js/jquery.datatables.js"></script>

<!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
<script src="../../templates/paper-dashboards/js/paper-dashboard.js"></script>

<!--  Notifications Plugin    -->
<script src="../../templates/paper-dashboards/js/bootstrap-notify.js"></script>

<script type="text/javascript">
	
	/*
     |--------------------------------------------------------------------------
     | CUSTOM NOTIFY MESSAGE
     |--------------------------------------------------------------------------
     |
     | message == message to shown
     | type ==  success || danger || warning || info
     | reload == reload page if needed || default is false
     |
     */
    function displayMessage(message, type = 'success', reload = false){

        var iconShow = 'ti-check-box';
        if(type == 'error')
        	type = 'danger';
        
        switch(type) {
            case "error":
                iconShow = 'ti-na';
                break;
            case "warning":
                iconShow = "ti-na";
                break;
            case "info":
                iconShow = "ti-info-alt";
                break;
        }

        $.notify({
            icon: iconShow,
            message: message
        },{
            type: type,
            timer: 1000,
            placement: {
                from: 'top',
                align: 'right'
            }
        });

        if(reload){
            setTimeout(function() {
                location.reload(true);
            }, 1000);
        }
    }

</script>
