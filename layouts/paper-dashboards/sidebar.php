<?php

$directoryURI = $_SERVER['REQUEST_URI'];
$path = parse_url($directoryURI, PHP_URL_PATH);
$components = explode('/', $path);
$first_part = $components[1];



?>

<div class="sidebar" data-background-color="brown" data-active-color="danger">
        <!--
    Tip 1: you can change the color of the sidebar's background using: data-background-color="white | brown"
    Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
-->
    <div class="logo">
        <a href="" class="simple-text logo-mini">
            S
        </a>

        <a href="" class="simple-text logo-normal">
            Synchroweb
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class=" btn-magnify <?php echo $first_part == 'dashboard' ? 'active' : '' ?>">
                <a href="../../dashboard">
                    <i class="ti-layout-grid2-alt"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="<?php echo $first_part == 'notes' ? 'active' : '' ?>">
                <a data-toggle="collapse" href="#user-management" aria-expanded="true">
                    <i class="ti-panel"></i>
                    <p>Note Management
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse <?php echo $first_part == 'notes' ? 'in' : '' ?>" id="user-management">
                    <ul class="nav">
                        <li class="<?php echo $first_part == 'notes' ? 'active' : '' ?>">
                            <a href="../../notes/">
                                <span class="sidebar-mini">T</span>
                                <span class="sidebar-normal">Note</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>