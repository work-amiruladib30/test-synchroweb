
<?php

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


if(isset($_POST['action'])){

	if($_POST['action'] == 'chart'){

		$spreadsheet = new Spreadsheet();

		$inputFileType = 'Xlsx';
		$inputFileName = 'test-data.xlsx'; //GET FROM HAKIM

		/**  Create a new Reader of the type defined in $inputFileType  **/
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
		/**  Advise the Reader that we only want to load cell data  **/
		$reader->setReadDataOnly(true);

		/**  Get total sheet   **/
		$worksheetData = $reader->listWorksheetInfo($inputFileName);

		##LOOP ALL SHEET
		foreach ($worksheetData as $worksheet) {
			
			##GET SHEET NAME
			$sheetName = $worksheet['worksheetName'];

			/**  Load $inputFileName to a Spreadsheet Object  **/
			$reader->setLoadSheetsOnly($sheetName);
			$spreadsheet = $reader->load($inputFileName);

			$worksheet = $spreadsheet->getActiveSheet();
				

			##SHEET 1
			if($sheetName == 'Passengger By Class'){
				if($_POST['sheet'] == 'Passengger By Class'){

					foreach ($worksheet->toArray() as $key => $value) {
						
						if($key != 0){
							$x_axis[] = $value[0];
						
							$row_data[0][] = $value[1];
							$row_data[1][] = $value[2];
							$row_data[2][] = $value[3];

						}
					}

					$data_sheet_1['sheet_1'] = [$row_data, $x_axis];
					## RETURN Response
					header('Content-type: application/json');
					echo json_encode($data_sheet_1['sheet_1']);
					continue;
				}
			}

			##SHEET 2
			if($sheetName == 'Visitor via Social Media'){
				if($_POST['sheet'] == 'Visitor via Social Media'){

					$pie_data[] = ['social_media', 'user'];
				
					foreach ($worksheet->toArray() as $key => $value) {
							
						// $pie_data[$value[0]] = $value[1];
						$pie_data[] = [$value[0], $value[1]];
						// $pie_data = array_merge($pie_data[0], $pie_data_append);

					}

					## RETURN Response
					header('Content-type: application/json');
					echo json_encode($pie_data);
					continue;

				}
			}

		}

	}
}


?>
