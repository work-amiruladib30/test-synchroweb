
<?php
	// Renama Page Name
	$page_name = 'Dashboard';
    include_once("../layouts/paper-dashboards/header.php");
?>

<div class="row">

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Activity</h4>
                <p class="category">Passenger By Class</p>
            </div>
            <div class="card-content">
                <div id="chartActivity"></div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Activity</h4>
                <p class="category">Visitor via social Media</p>
            </div>
            <div class="card-content">
                <div id="piechart"></div>
            </div>
        </div>
    </div>
</div> <!-- end row -->

<?php
    include_once("../layouts/paper-dashboards/footer.php");
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

	$(document).ready(function(){

		$.ajax({
           	type: 'POST',
            url	: 'process.php',
            data: {'action' : 'chart', 'sheet' : 'Passengger By Class'},
            success: function (data) {
            	drawGraphChart(data[1], data[0]);
            }
        });
    

        $.ajax({
           	type: 'POST',
            url	: 'process.php',
            data: {'action' : 'chart', 'sheet' : 'Visitor via Social Media'},
            success: function (data) {
            	drawPieChart(data);
            }
        });
	})

	function drawGraphChart(x_axis, row_data){
		var data = {
	      labels: x_axis,
	      series: row_data,
	    };

	    var options = {
	        seriesBarDistance: 10,
	        axisX: {
	            showGrid: true
	        },
	        height: "250px"
	    };

	    var responsiveOptions = [
	      ['screen and (max-width: 640px)', {
	        seriesBarDistance: 5,
	        axisX: {
	          labelInterpolationFnc: function (value) {
	            return value[0];
	          }
	        }
	      }]
	    ];

	    Chartist.Bar('#chartActivity', data, options, responsiveOptions);
	}

	function drawPieChart(data_array){
		 //PIE CHART 
		// Load google charts
		google.charts.load('current', {'packages':['corechart']});
		google.charts.setOnLoadCallback(drawChart);

		// Draw the chart and set the chart values
		function drawChart() {
			var data = google.visualization.arrayToDataTable(data_array);

			// Optional; add a title and set the width and height of the chart
			var options = {'title':'Visitor via Social Media', 'width':550, 'height':400, 'sliceVisibilityThreshold' : .0};

			// Display the chart inside the <div> element with id="piechart"
			var chart = new google.visualization.PieChart(document.getElementById('piechart'));
			chart.draw(data, options);
		}
		
	}

   
</script>